var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var cPushArray = [canvas.toDataURL()];
var cStep = 0;
var brush_size = 0;
var eraser_bool = false;
var word_bool = false;
var color_bool = true;
var c_bool = false;
var t_bool = false;
var r_bool = false;
var text = 10;
var font = "Verdana";
var t_max_x = 0;
var r_max_x = 0;
ctx.strokeStyle = "black";

function reset() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for(let i = 0; i < cPushArray.length; ++i)
    {
        cPushArray.pop();
    }
    cStep = 0;
    word_bool = false;
    c_bool = false;
    t_bool = false;
    r_bool = false;
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('painting.png'), auto";
    ctx.font = 10 + "px " + "Verdana";
    ctx.fillStyle = "black";
    ctx.strokeStyle = "black";
    brush_size = 1;
    changeBrushSize(brush_size);
    t_max_x = 0;
    r_max_x = 0;
}

function draw() {
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('painting.png'), auto";
    word_bool = false;
    eraser_bool = false;
    c_bool = false;
    t_bool = false;
    r_bool = false;
}

function erase() {
    eraser_bool = true;
    ctx.strokeStyle = "white";
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('eraser.png'), auto";
    word_bool = false;
    c_bool = false;
    t_bool = false;
    r_bool = false;
}

function word() {
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('word.png'), auto";
    word_bool = true;
    eraser_bool = false;
    c_bool = false;
    t_bool = false;
    r_bool = false;
}

function font_size() {
    document.addEventListener('keydown', function(evt) {
        if(evt.keycode === 13 || evt.which === 13)
        {
            text = document.getElementById("font_size").value;
            ctx.font = text + "px " + font;
        }
    }, false);
}

function typeface() {
    font = document.getElementById("font").value;
    console.log(font);
    ctx.font = text + "px " + font;
}

function color() {
    if(color_bool && !eraser_bool)
    {
        for (let i = 0; i < 5; ++i) {
            for (let j = 0; j < 5; ++j) {
                for(let k = 0; k < 5; ++k) {
                    let color_ = document.createElement("button");
                    color_.id = "color_";
                    color_.name = "color_"
                    color_.style.backgroundColor = 'rgb(' + Math.floor(255 - 42.5 * i) + ', ' + Math.floor(255 - 42.5 * j) + ', ' + Math.floor(255 - 42.5 * k) + ')';
                    color_.style.borderRadius = "9999px";
                    color_.style.padding = "8px";
                    color_.style.margin = "6px";
                    color_bool = false;
                    color_.onclick = function () {
                        ctx.strokeStyle = color_.style.backgroundColor;
                        ctx.fillStyle = color_.style.backgroundColor;
                        for(let l = 0; l < 125; ++l)
                        {
                            let obj = document.getElementById("color_");
                            let parent = obj.parentNode;
                            parent.removeChild(obj);
                            color_bool = true;
                        }
                    }
                    document.body.appendChild(color_);
                }
            }
        }
    }
    else
    {

    }
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect(); //get the position of bounding
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

function mouseMove(evt) {
    if(!c_bool && !r_bool && !t_bool)
    {
        var mousePos = getMousePos(canvas, evt);
        ctx.lineTo(mousePos.x, mousePos.y); 
        ctx.stroke(); // draw
    }
}

canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    if(word_bool)
    {
        let x = mousePos.x;
        let y = mousePos.y;

        let words = document.createElement("input");
        words.type = "text";
        words.id = "word_";
        words.style.position = "absolute";
        words.style.left = x + "px";
        words.style.top = y + "px";
        words.style.zIndex = "1";
        words.onblur = function () {
        let obj = document.getElementById("word_");
        let text = obj.value;
        if (text !== "") {
            ctx.fillText(text, x, y);
            cPush();
        }
        obj.parentNode.removeChild(obj);
        };

        document.body.appendChild(words);

        $(document).ready(function() {
            $("#word_").focus();
        });
        // let words = document.createElement("input");
        // words.type = "text";
        // words.id = "word_";
        // words.style.position = "absolute";
        // words.style.left = mousePos.x + "px";
        // words.style.top = mousePos.y + "px";
        // let x = mousePos.x;
        // let y = mousePos.y;
        // words.style.zIndex = "1";
        // words.autofocus = "true";
        // document.body.appendChild(words);
        // words.onblur = function () {
        //     let text = document.getElementById("word_").value;
        //     ctx.fillText(text, x, y);
        //     cPush();
        //     obj = document.getElementById("word_");
        //     parent = obj.parentNode;
        //     parent.removeChild(obj);
        // };
    }
    else
    {
        ctx.beginPath(); //new path
        ctx.moveTo(mousePos.x, mousePos.y);
        evt.preventDefault();
        canvas.addEventListener('mousemove', mouseMove, false); //false for bubbling
    }
});

canvas.addEventListener('mouseup', function() {
    canvas.removeEventListener('mousemove', mouseMove, false);
    cPush();
}, false);

$('.container input').change(function(){
    brush_size = $('#brush_size').val();
    if(brush_size == 0) brush_size = 1;
    changeBrushSize(brush_size);
});

function changeBrushSize(brush_size){
    ctx.lineWidth = brush_size * 0.2;
};

$('#save').on('click', function(){
    var _url = canvas.toDataURL(); //convert canvas to image
    this.href = _url; //let data:image be href
});

function circle() {
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('painting.png'), auto";
    let c_num = 0;
    c_bool = true;
    r_bool = false;
    t_bool = false;
    word_bool = false;
    eraser_bool = false;
    var radius = 50;
    var startX = 0;
    var startY = 0;
    var circle_bool = false;
    var putPoint = function(e){
        if(c_bool)
        {
            startX = e.clientX;
            startY = e.clientY;
            circle_bool = true;
            radius = 0;
        }
    }
    var drawPoint = function(e){
        if(!circle_bool) return;
        if(c_bool)
        {
            let x = startX - e.clientX;
            let y = startY - e.clientY;
            radius = Math.sqrt(x * x + y * y)
            ctx.beginPath();
            ctx.arc(startX, startY, radius, 0, Math.PI*2);
            ctx.fill();
        }
    }
    var stopPoint = function(e){
        if(c_bool)
        {
            if(c_num)
            {
                cStep--;
                c_num++;
                cPush();
            }
            else c_num++;
            circle_bool = false;
        }
    }
    if(c_bool)
    {
        canvas.addEventListener('mousedown',putPoint);
        canvas.addEventListener('mousemove',drawPoint);
        canvas.addEventListener('mouseup',stopPoint);
    }
}

function rectangle() {
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('painting.png'), auto";
    let r_num = 0;
    r_bool = true;
    c_bool = false;
    t_bool = false;
    word_bool = false;
    eraser_bool = false;
    var startX = 0;
    var startY = 0;
    var rectangle_bool = false;
    var putPoint = function(e){
        if(r_bool)
        {
            startX = e.clientX;
            startY = e.clientY;
            rectangle_bool = true;
        }
    }
    var drawPoint = function(e){
        if(!rectangle_bool) return;
        if(r_bool)
        {
            console.log("here");
            let x = e.clientX - startX;
            let y = e.clientY - startY;
            ctx.beginPath();
            ctx.rect(startX, startY, x, y);
            ctx.fill();
        }
    }
    var stopPoint = function(e){
        if(r_bool)
        {
            if(r_num)
            {
                cStep--;
                r_num++;
                cPush();
            }
            else r_num++;
            rectangle_bool = false;
        }
    }
    if(r_bool)
    {
        canvas.addEventListener('mousedown',putPoint);
        canvas.addEventListener('mousemove',drawPoint);
        canvas.addEventListener('mouseup',stopPoint);
    }
}

function triangle() {
    let elementToChange = document.getElementById("canvas");
    elementToChange.style.cursor = "url('painting.png'), auto";
    let t_num = 0;
    t_bool = true;
    c_bool = false;
    r_bool = false;
    word_bool = false;
    eraser_bool = false;
    var startX = 0;
    var startY = 0;
    var triangle_bool = false;
    var putPoint = function(e){
        if(t_bool)
        {
            startX = e.clientX;
            startY = e.clientY;
            triangle_bool = true;
        }
    }
    var drawPoint = function(e){
        if(!triangle_bool) return;
        if(t_bool)
        {
            console.log(e.clientX, t_max_x);
            if((e.clientX - startX) < 0)
            {
                if(t_max_x > startX)
                {
                    if(Math.abs(e.clientX - startX) > t_max_x - startX)
                    {
                        t_max_x = e.clientX;
                    }
                }
                else
                {
                    if(Math.abs(e.clientX - startX) > Math.abs(t_max_x - startX))
                    {
                        t_max_x = e.clientX;
                    }
                }
            }
            else
            {
                if(t_max_x > startX)
                {
                    if(Math.abs(e.clientX - startX) > t_max_x - startX)
                    {
                        t_max_x = e.clientX;
                    }
                }
                else
                {
                    if(Math.abs(e.clientX - startX) > Math.abs(t_max_x - startX))
                    {
                        t_max_x = e.clientX;
                    }
                }
            }
            if(Math.abs(e.clientX) > Math.abs(t_max_x)) t_max_x = e.clientX;
            ctx.beginPath();
            ctx.moveTo(startX, startY);
            ctx.lineTo(t_max_x, e.clientY);
            ctx.lineTo((2*startX - t_max_x), e.clientY);
            ctx.fill();
        }
    }
    var stopPoint = function(e){
        if(t_bool)
        {
            if(t_num)
            {
                cStep--;
                t_num++;
                cPush();
            }
            else t_num++;
            triangle_bool = false;
            t_max_x = 0;
        }
    }
    if(t_bool)
    {
        canvas.addEventListener('mousedown',putPoint);
        canvas.addEventListener('mousemove',drawPoint);
        canvas.addEventListener('mouseup',stopPoint);
    }
}

function cPush() {
    cStep++;
    if (cStep < cPushArray.length) { cPushArray.length = cStep; }
    cPushArray.push(document.getElementById("canvas").toDataURL());
}

function cUndo() {
    if (cStep > 0) {
        cStep--;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () {
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}
function cRedo() {
    if (cStep < cPushArray.length-1) {
        cStep++;
        var canvasPic = new Image();
        canvasPic.src = cPushArray[cStep];
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvasPic.onload = function () {
            ctx.drawImage(canvasPic, 0, 0);
        }
    }
}


