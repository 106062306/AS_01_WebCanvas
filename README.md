# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

1.font_size: 第一個default為10的input 改掉數字後 按下enter就能更換文字大小
2.typeface: 下拉是選單 按選後即能改變文字字體
3.brush_size: 拖曳方框即能改變筆刷大小
4.color: 按下後下方會出現125種顏色 選按任一種顏色後 125個button就會消失 且筆刷顏色變更為剛剛點選按鈕的顏色
5.draw: 畫畫模式 cursor為一隻手拿筆刷
6.eraser: 橡皮擦模式 cursor為橡皮擦
7.word: 在canvas中按下滑鼠 就會出現一個input框 可輸入文字 輸入完按旁邊任一處即可將文字滑入圖中
8.circle: 先按滑鼠兩下即可拖曳
9.rectangle: 先按滑鼠兩下即可拖曳
10.triangle: 先按滑鼠兩下即可拖曳
11.undo: 清除上一個動作
12.redo: 回覆上一個動作
13.save canvas: 儲存現在canvas上的畫面到電腦
14.reset: 將canvas上所有東西清空